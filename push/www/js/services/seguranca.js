angular.module('app')
.service('SEGURANCA', function($rootScope){

  var secretKey = 'U4l@b1T3c44m0b1';

  this.estaLogado = function(){
    this.leDados();
    if($rootScope.usuarioLogadoDados){
      return true;
    }
    else {
      return false;
    }
  };

  this.getToken = function(){
    if(!this.leDados()) {
      return '';
    }
    else {
      return $rootScope.usuarioLogadoDados.SecurityToken;
    };
  };

  this.dadosUsuarioLogado = function() {
    //console.log('SEGURANCA.dadosUsuarioLogado');
    this.leDados();
    return $rootScope.usuarioLogadoDados;
    //console.log('SEGURANCA.dadosUsuarioLogado.usuarioLogadoDados: ' + $rootScope.usuarioLogadoDados);
  }

  this.gravaLogin = function(usuarioDados) {
    //console.log('SEGURANCA.gravaLogin: ' + usuarioDados);
    $rootScope.usuarioLogadoDados = usuarioDados;
    this.salvaDados($rootScope.usuarioLogadoDados);
  }

  this.efetuarLogoff = function() {
    //console.log('SEGURANCA.efetuarLogoff');
    $rootScope.usuarioLogadoDados = undefined;
    this.salvaDados(undefined);
  }


  this.salvaDados = function(dados) {
    if(dados){
      localStorage.setItem("dadosLogin", JSON.stringify(dados));
      //console.log('Criptografou');
      //localStorage.setItem("dadosLogin", CryptoJS.AES.encrypt(angular.toJson(dados), secretKey).toString());
    }
    else{
      localStorage.removeItem("dadosLogin");
    }
  }

  this.leDados = function() {
    //console.log('SEGURANCA.leDados');

    if(!$rootScope.usuarioLogadoDados){

      //console.log('SEGURANCA.leDados IF: ' + angular.toJson(localStorage.getItem('dadosLogin')));

      if(localStorage.getItem('dadosLogin') != null)
      {
        $rootScope.usuarioLogadoDados = JSON.parse(localStorage.getItem('dadosLogin'));

        //console.log('SEGURANCA.leDados Parse');

        //$rootScope.usuarioLogadoDados = angular.fromJson(CryptoJS.AES.decrypt(localStorage.getItem('dadosLogin'), secretKey).toString(CryptoJS.enc.Utf8));

        if($rootScope.usuarioLogadoDados){
          //console.log('SEGURANCA.leDados: ' + JSON.stringify($rootScope.usuarioLogadoDados));
          return true;
        }
        else {
          //console.log('SEGURANCA.leDados false');
          return false;
        }
      }
      else {
        //console.log('SEGURANCA.leDados false');
        return false;
      }
    }
    else {
      //console.log('SEGURANCA.leDados true2');
      return true;
    }
  }
})
