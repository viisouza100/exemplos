angular.module('app')
.service('USUARIOS', function($http,appConfig){
    this.getToken = function(requestData){

      var data = 'grant_type=password&username=' + requestData.login + '&password=' + requestData.password;
      console.log("appConfig");
      console.log(appConfig);
      return $http.post(appConfig.url + 'security/token',
                        data,
                        {
                          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
                        });
    };

    this.getUserData = function(token){
        return $http.get(appConfig.url + 'acessoapp', { headers: { 'Authorization': 'Bearer ' + token } });
    };

    this.recuperaSenha = function(requestData){
        return $http.post(appConfig.url + 'RememberPwd', requestData);
    };

    this.listarValues = function(idUser){

      return $http.get(appConfig.url + 'User/GetValues?idUser=' + idUser);
    };

    this.updStudent = function(pIdUser, pIdStudent, pDados) {

      var postDados = {
        IdUser: pIdUser,
        IdStudent: pIdStudent,
        IdSchool: pDados.idSchool,
        Name1: pDados.Name1,
        Name2: pDados.Name2,
        NickName: pDados.NickName,
        DtBirth: pDados.DtBirthStamp,
        imgProfile: pDados.imgProfile,
        RegNum: pDados.RegNum,
        Sex: pDados.Sex
      };

      //console.log('USUARIO.updStudent.postDados: ' + JSON.stringify(postDados));

      return $http.put(appConfig.url + 'Student', postDados);
    };

    this.updUserApp = function(pIdUser, pDados) {

      var postDados = {
        idUser: pIdUser,
        Name1: pDados.Name1,
        Name2: pDados.Name2,
        Login: pDados.Login,
        DtBirth: pDados.DtBirthStamp,
        Sex: pDados.Sex,
        imgProfile: pDados.imgProfile
      };

      //console.log('USUARIO.updUserApp.postDados: ' + JSON.stringify(postDados));

      return $http.put(appConfig.url + 'UserApp', postDados);
    };

    this.updPassword = function(pIdUser, pDados) {

      var postDados = {
        idUser: pIdUser,
        Password: pDados.senhaAtual,
        NewPassword: pDados.novaSenha,
        ConfirmNewPassword: pDados.confNovaSenha
      };

      //console.log('USUARIO.updPassword.postDados: ' + JSON.stringify(postDados));

      return $http.post(appConfig.url + 'UserApp', postDados);
    };
});
